module FilterBank

using Unitful
using AxisKeys
using UnalignedVectors
using Statistics: mean
import Mmap


mutable struct FBFile
    path::String
    header::Dict{String, Any}
    data_offset::Int
    data::Any
    
    function FBFile(path::String)
        fb = new(path)
        read_header!(fb)
        fb.data = mmap(fb)
        return fb
    end
end

function read_header!(fb_file::FBFile)
    function get_field(io::IOStream)
        read_value(io::IOStream, ::Type{Nothing}) = nothing
        read_value(io::IOStream, ::Type{Int32}) = read(io, Int32)
        read_value(io::IOStream, ::Type{Float64}) = read(io, Float64)
        read_value(io::IOStream, ::Type{String}) = String(read(io, read(io, Int32)))
        
        type_map = merge(
            Dict(k => Nothing for k in ["HEADER_START", "HEADER_END"]),
            Dict(k => String for k in ["rawdatafile", "source_name"]),
            Dict(k => Int32 for k in ["machine_id", "telescope_id", "data_type", "nchans", "nbeams", "ibeam", "nbits", "nifs"]),
            Dict(k => Float64 for k in ["src_raj", "src_dej", "az_start", "za_start", "fch1", "foff", "tstart", "tsamp"]),
        )
        
        len = read(io, Int32)
        if len > 20
            throw(BoundsError(len))
        end
        name = String(read(io, len))
        ftype = type_map[name]
        return name, read_value(io, ftype)
    end

    fb_file.header = Dict()
    open(fb_file.path, "r") do f
        while true
            k, v = get_field(f)
            fb_file.header[k] = v
            if k == "HEADER_END" break end
        end
        
        fb_file.data_offset = position(f)
    end
end

function mmap(fb_file::FBFile)
    nchans = Int(fb_file.header["nchans"])
    f = open(fb_file.path, "r")
    seekend(f)
    nbytes = position(f) - fb_file.data_offset
    ntimes = nbytes ÷ (nchans * 4)
    seekstart(f)
    mmap = Mmap.mmap(f, Array{UInt8, 1}, 4 * ntimes * nchans, fb_file.data_offset)
    v = UnalignedVector{Float32}(mmap)
    v = reshape(v, (nchans, ntimes))
    timestep = fb_file.header["tsamp"]u"s"
    timestep = round(Int, u"μs", timestep)
    return KeyedArray(
        v,
        freq=range(fb_file.header["fch1"], step=fb_file.header["foff"], length=nchans) * u"MHz",
        time=range(0u"μs", step=timestep, length=ntimes))
end


Base.@kwdef struct FBFiles
    header::Dict{String, Any}
    data::Dict{String, Dict{String, AbstractArray{Float32, 2}}}
end


function FBFiles(paths::Dict{NamedTuple{(:pol, :compl)}, String})        
    data = Dict()
    header = nothing
    axes = nothing
    for (info, path) in pairs(paths)
        fb = FBFile(path)
        if header == nothing
            header = fb.header
        end
        @assert header == fb.header

        if axes == nothing
            axes = axiskeys(fb.data)
        end
        @assert axes == axiskeys(fb.data)

        get!(data, uppercase(info.pol), Dict())[lowercase(info.compl)] = fb.data
    end

    return FBFiles(header, data)
end

function FBFiles(dir::String, pattern::Regex)
    names = filter(f -> occursin(pattern, f), readdir(dir))
    paths = ["$dir/$name" for name in names]
    return FBFiles(Dict(path_info(p) => p for p in paths))
end

function path_info(path::String)
    m = match(r"/.*_(?<pol>[rliquv]{1,2})(?:_(?<compl>re|im))?\..*fb$"i, path)
    return NamedTuple{(:pol, :compl)}((m[:pol], something(m[:compl], "re")))
end

function peak_t_ix(sd::FBFiles)
    data = sd.data["RR"]["re"]
    means = dropdims(mean(data, dims=:freq), dims=:freq)
    peaks = findmax(means)
    return peaks[2]
end


include("specpol.jl")

end
