using Unitful
using AxisKeys
using Statistics: mean
using SplitApplyCombine: splitdimsview
using Utils: split_axis

# @inline function mymean(A)
#     x = zero(eltype(A))
#     cnt = 0
#     for a in A
#         x += a
#         cnt += 1
#     end
#     x / cnt
# end

# nchan(spec::KeyedArray) = size(spec, :freq) ÷ 8


function specpol(fbf::FBFiles; t_ix_base::Int, t_ix_cnt::Int, pols=[:RR, :LL, :RL])
    tmp = (fbf.data |> values |> first)["re"]
    @assert dimnames(tmp) == (:freq, :time)
    time_ix = t_ix_base - (t_ix_cnt - 1) ÷ 2:t_ix_base + t_ix_cnt ÷ 2
    times = axiskeys(tmp, :time)[time_ix] #.- axiskeys(tmp, :time)[t_ix_base]
    data_raw = Array{ComplexF32}(undef, size(tmp, :freq), length(time_ix), length(pols))
    data = KeyedArray(data_raw, freq=axiskeys(tmp, :freq), time=times, pol=pols)
    for pol in pols
        pol_data = fbf.data[string(pol)]
        if haskey(pol_data, "re") && haskey(pol_data, "im")
            pol_data = pol_data["re"][time=time_ix] + 1im * pol_data["im"][time=time_ix]
        else
            @assert haskey(pol_data, "re")
            pol_data = pol_data["re"][time=time_ix]
        end
        data(pol=pol) .= pol_data
    end
    times = axiskeys(tmp, :time)[time_ix] .- axiskeys(tmp, :time)[t_ix_base]
    data = KeyedArray(data_raw, freq=axiskeys(data, :freq), time=times, pol=axiskeys(data, :pol))
    return data
end

# times(sp) = axiskeys(sp, :time)
# freqs(sp) = axiskeys(sp, :freq)
# freqs_ifs(sp, n_ifs::Int) = reshape(freqs(sp), (:, n_ifs))

# reverse_freqs(sp) = sp[freq=Base.axes(sp, :freq) |> reverse]
    
function to_ifs(spec, n_ifs::Int)
    @assert dimnames(spec) == (:freq, :time, :pol)
    n_chan_per_if = size(spec, :freq) ÷ n_ifs
    @assert n_chan_per_if * n_ifs == size(spec, :freq)
    data = reshape(spec.data, n_chan_per_if, n_ifs, size(spec)[2:3]...)
    return KeyedArray(data, chan=1:n_chan_per_if, iif=1:n_ifs, time=axiskeys(spec, :time), pol=axiskeys(spec, :pol))
end
    
function average_chans(spec, n_avg::Int)
    @assert dimnames(spec) == (:freq, :time, :pol)
    spec_reshaped = split_axis(spec, :freq => (chan_avg=n_avg, freq=:), keys_unitrange=false)
    spec_avg = map(a -> filter(!isnan, a) |> mean, splitdimsview(spec_reshaped, (:freq, :time, :pol)))
    return spec_avg
end
    
function normalize_chans(spec)
    means = mean(spec, dims=Axis{:time})
    data_norm = spec ./ means
    return AxisArray(data_norm, spec.axes...)
end
    
# function imshow_s(spec::AxisArray; pol, func, kwargs...)
#     imshow(spec[Axis{:pol}(pol)].data .|> func, origin="lower",
#            extent=(minimum(S.times(spec) .|> u"ms").val, maximum(S.times(spec) .|> u"ms").val, minimum(S.freqs(spec)).val, maximum(S.freqs(spec)).val);
#            kwargs...)
#     gca().set_aspect.("auto")
# end
